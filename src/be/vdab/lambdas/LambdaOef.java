package be.vdab.lambdas;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdaOef {
    public static void main(String[] args) {
        int[] array = makeArray();

        Predicate<Integer> filter = i -> (i % 2 == 0) && (i % 7 == 0);
        Consumer<Integer> consumer = System.out::println;
        printFilteredIntegers(filter, consumer, array);
    }

    public static int[] makeArray() {
        List<Integer> intList = new ArrayList<>();
        for(int i = 0; i < 100; i++) {
            intList.add(i);
        }

        return intList.stream().mapToInt(i -> i).toArray();
    }

    public static void printFilteredIntegers(Predicate<Integer> filter, Consumer<Integer> consumer, int[] array) {

        for(Integer i : array) {
            if(filter.test(i)) {
                consumer.accept(i);
            }
        }
    }
}