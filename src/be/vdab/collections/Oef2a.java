package be.vdab.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Oef2a {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Give a sentences (1/2): ");
        Stream<String> sentenceOneStream = Arrays.stream(scanner.nextLine().split(" ")).map(String::toLowerCase);

        System.out.print("Give a sentence (2/2): ");
        List<String> sentenceTwoList = Arrays.stream(scanner.nextLine().split(" ")).map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new));

        filterWords(sentenceOneStream, sentenceTwoList);
    }

    public static void filterWords(Stream<String> one, List<String> two) {
        one.filter(w -> !two.contains(w)).map(String::toUpperCase).forEach(System.out::println);
    }
}