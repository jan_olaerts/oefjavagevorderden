package be.vdab.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class Oef2b {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        System.out.print("Give a sentences (1/2): ");
        List<String> sentenceOne = Arrays.stream(scanner.nextLine().split(" ")).map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new));

        System.out.print("Give a sentence (2/2): ");
        List<String> sentenceTwo = Arrays.stream(scanner.nextLine().split(" ")).map(String::toLowerCase).collect(Collectors.toCollection(ArrayList::new));

        filterAndChange(sentenceOne, sentenceTwo);
    }

    public static void filterAndChange(List<String> one, List<String> two) {
        one.stream().filter(w -> !two.contains(w)).sorted().forEach(word -> {
            word = word
                    .replace('e', '3')
                    .replace('a', '4')
                    .replace('i', '1')
                    .replace('o', '0');
            System.out.println(word);
        });
    }
}