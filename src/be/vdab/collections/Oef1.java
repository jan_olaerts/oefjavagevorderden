package be.vdab.collections;

import java.util.*;

public class Oef1 {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.print("Give a sentence: ");
        String sentence = scanner.nextLine();
        String[] words = sentence.split(" ");
        List<String> wordList = new LinkedList<>(Arrays.asList(words));

        countLetters(wordList);
    }

    public static void countLetters(List<String> wordList) {
        Map<Character, Integer> letterCounts = new TreeMap<>();

        wordList.forEach(item -> {
            for(int i = 0; i < item.length(); i++) {
                item = item.toLowerCase();
                char c = item.charAt(i);
                int count = letterCounts.getOrDefault(c, 0);
                letterCounts.put(c,count + 1);
            }
        });

        letterCounts.forEach((k,v) -> System.out.println("\"" + k + "\"" + " : " + v));
    }
}